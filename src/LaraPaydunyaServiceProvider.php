<?php

namespace Volkeno\LaraPaydunya;

use Illuminate\Support\ServiceProvider;

class LaraPaydunyaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if(!$this->app->routesAreCached()){
            $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        }

        
    }
}
